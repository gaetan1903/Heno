import 'package:flutter/material.dart';
import 'package:heno/widgets/infoPage.dart';
import 'package:heno/widgets/mainDrawer.dart';



class MyHomePage extends StatefulWidget {
	MyHomePage({Key key, this.title}) : super(key: key);

	final String title;

	@override
	_MyHomePageState createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
	bool option1 = false;
	bool option2 = false;
	bool option3 = false;
	bool option4 = false;  


	@override
	Widget build(BuildContext context) {
		return Scaffold(
      key: _scaffoldKey,
			appBar: AppBar(
				title: Text(widget.title),
				leading: IconButton(
					icon: Icon(Icons.menu),
					tooltip: "Bouton de menu",
					onPressed: (){
						print('hey');
            _scaffoldKey.currentState.openDrawer();
					},
				),
				actions: <Widget>[
					IconButton(
						icon: Icon(Icons.help),
						tooltip: "Aidez-moi",
						onPressed: (){
							print("hello world !");
              Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context){
                  return InfoPage();
                }));
            }
          )
,
				],
			),

      drawer: Drawer(
        child: SizedBox(
          child:MainDrawer()
        )
      ),
		
			////////////////////////////////////////////////////Body/////////////////////////////////////////////////////////////////////////
			body: Container(
				child: Column(
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						
						////////////////////////////////////Card1//////////////////////////////////////////////////////////////
						Card(
							elevation: 15,
							margin: EdgeInsets.all(10),
							child: Container(
								width: MediaQuery.of(context).size.width,
								height: MediaQuery.of(context).size.height/6,
								child: ListTile(
									leading: FlutterLogo(),
									title: Container(
										padding: EdgeInsets.only(top:5),
										child: Text("Positionner et Alerter"),
									),
									trailing: Switch(
										value: option1,
										onChanged: (bool newValue) {
											setState(() {
												print(newValue);
												if (newValue==true){
													option1 = true;
													option2 = false;
													option3 = false;
													option4 = false;
												}
												else{
												option1 = false;
												}
											});
										}
									),
									subtitle: Container(
										padding: EdgeInsets.only(top: 10),
										child: Text("Activer ceci et metter le dans votre poche."),
									)			
								),
							)
						),

						//////////////////////////////////////Card2///////////////////////////////////////////////////////
						Card(
							elevation: 15,
							margin: EdgeInsets.all(10),
							child: Container(
								width: MediaQuery.of(context).size.width,
								height: MediaQuery.of(context).size.height/6,
								child: ListTile(
									leading: FlutterLogo(),
									title: Container(
										padding: EdgeInsets.only(top:5),
										child: Text("Detecter et Alerter"),
									),
									trailing: Switch(
										value: option2,
										onChanged: (bool newValue) {
											setState(() {
												print(newValue);
												if (newValue==true){
													option1 = false;
													option2 = true;
													option3 = false;
													option4 = false;
												}
												else{
													option2 = false;
												}
											});
										}
									),
									subtitle: Container(
										padding: EdgeInsets.only(top: 10),
										child: Text("Active l'alerte si il est dans une poche"),
									)
								),
							)
						),

						////////////////////////////////Card3///////////////////////////////////////////////////////////////
						Card(
							elevation: 15,
							margin: EdgeInsets.all(10),
							child: Container(
								width: MediaQuery.of(context).size.width,
								height: MediaQuery.of(context).size.height/6,
								child: ListTile(
									leading: FlutterLogo(),
									title: Container(
										padding: EdgeInsets.only(top:5),
										child: Text("Reconnaître et Alerter"),
									),
									trailing: Switch(
										value: option3,
										onChanged: (bool newValue) {
											setState(() {
												print(newValue);
												if (newValue==true){
													option1 = false;
													option2 = false;
													option3 = true;
													option4 = false;
												}
												else{
													option3 = false;
												}
											});
										}
									),
									subtitle: Container(
										padding: EdgeInsets.only(top:10),
										child: Text("Reconnaitre le propriétaire en menntant dans la poche"),
									)
								),
							)
						),

						////////////////////////////////Card4/////////////////////////////////////////////////////////
						Card(
							elevation: 15,
							margin: EdgeInsets.all(10),
							child: Container(
								width: MediaQuery.of(context).size.width,
								height: MediaQuery.of(context).size.height/6,
								child: ListTile(
									leading: FlutterLogo(),
									title: Container(
										padding: EdgeInsets.only(top:5),
										child: Text("Detecter, Reconnaître et Alerter"),
									),
									trailing: Switch(
										value: option4,
										onChanged: (bool newValue) {
											setState(() {
												print(newValue);
												if (newValue==true){
													option1 = false;
													option2 = false;
													option3 = false;
													option4 = true;
												}
												else{
													option4 = false;
												}
											});
										}
									),
									subtitle: Container(
										padding: EdgeInsets.only(top:10),
										child: Text("Detection de poche et de propriétaire"),
									)
								),
							)
						),
					],
				),
			),
		);
	}
}
