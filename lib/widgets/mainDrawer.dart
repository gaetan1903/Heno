import 'package:flutter/material.dart';
import 'package:heno/widgets/signupPage.dart';
import 'package:heno/widgets/loginPage.dart';


class MainDrawer extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Drawer(
      child: Column(children: <Widget>[
        Container( 
          width: double.infinity,
          color: Theme.of(context).primaryColor,
          child: UserAccountsDrawerHeader(
            currentAccountPicture: Icon(Icons.account_circle, size:72, color: Colors.white),
            accountName: Container(
              margin: EdgeInsets.only(top:25),
              child: Row(
                children: <Widget>[
                  Icon(Icons.account_box, color: Colors.white),
                  Text('  Gaetan Jonathan')
                ]
                )
              ),
            accountEmail: Container(
              margin: EdgeInsets.only(top:5),
              child: Row(
                children: <Widget>[
                  Icon(Icons.mail, color: Colors.white,),
                  Text("  gaetan.joanthan.bakary@esti.mg")
                ],
              )
            ),    
          )

        ),

        Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 15),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Colors.transparent,
                  onPressed: (){
                    Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context){
                      return LoginPage();
                    }));
                  },
                  elevation: 15,
                  padding: EdgeInsets.all(0),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 60,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFF0D47A1),
                          Color(0xFF1976D2),
                          Color(0xFF42A5F5),
                        ],
                      ),
                      borderRadius: BorderRadius.circular(20)
                    ),
                    child: Center(
                      child:
                        Text('Connexion', style: TextStyle(fontSize: 24),)
                    )    
                  )
                )
              ),

              Container(
                margin: EdgeInsets.only(bottom: 15),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Colors.transparent,
                  onPressed: (){
                    Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context){
                      return SignupPage();
                    }));
                  },
                  elevation: 15,
                  padding: EdgeInsets.all(0),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 60,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFF0D47A1),
                          Color(0xFF1976D2),
                          Color(0xFF42A5F5),
                        ],
                      ),
                      borderRadius: BorderRadius.circular(20)
                    ),
                    child: Center(
                      child:
                        Text("Inscription", style: TextStyle(fontSize: 24),)
                    )    
                  )
                )
              ),
              
            ],
          )
        )
      ],)
    );
  }
}