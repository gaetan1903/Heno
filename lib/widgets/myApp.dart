import 'package:flutter/material.dart';
import 'package:heno/widgets/home.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Heno',
			theme: ThemeData(
				primarySwatch: Colors.teal,
			),
			debugShowCheckedModeBanner: false,
			home: MyHomePage(title: 'Heno'),
		);
	}
}