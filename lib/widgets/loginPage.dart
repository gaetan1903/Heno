import 'package:flutter/material.dart';


class LoginPage extends StatefulWidget{
  @override 
  _LoginPageState createState() => _LoginPageState();
}


class _LoginPageState extends State<LoginPage>{
  @override 
  Widget build(BuildContext context){
    return  Scaffold(
      appBar: AppBar(
        title: Text("Connexion"),
      ),
      body: Center(
        widthFactor: MediaQuery.of(context).size.width ,
        child: Card(
          elevation: 15,
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: MediaQuery.of(context).size.height * 0.5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Se connecter à Heno", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                Container(
                  margin: EdgeInsets.all(20),
                  child: Form(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(
                            hintText: "gaetan1903@gmail.com",
                            labelText: "Entrer Votre adresse"
                          ),
                          keyboardType: TextInputType.emailAddress,
                        ),

                        SizedBox(height: 8),

                        TextFormField(
                          decoration: InputDecoration(
                            hintText: "6 caractères minimum",
                            labelText: "Entrer mot de passe"
                          ),
                          obscureText: true,
                        ),

                        SizedBox(height: 15),

                        RaisedButton (
                          textColor: Colors.white,
                          onPressed: (){},
                          color: Colors.teal,
                          child: Text("Se connecter", style:TextStyle(fontSize: 16)),
                        )
                      ],
                    )
                  )
                )
              ],
            )
            )
        )
      )
    );

  }
}
